@Register
Feature: Register

 Scenario Outline: User can register an account with <condition>
    Given User go to register page 
    When User input name field with <name> name in Register Page
    And User input email field with <email> email in Register Page
    And User input password field with <password> password in Register Page
    And User click Daftar button
    Then User <result> regist

    Examples:
    | case_id |	condition						|	name		|	email			| password		|	result 				|
    | B01		 	| correct term				| valid		| valid			| valid 			| successfully	|
    | B02		 	| name field empty		| empty		| valid			|	valid 			| failed				|
    | B03		 	| registered email		| valid		| invalid		| valid 			| failed				|
    | B04			| email field empty		|	valid		| empty 		| valid 			| failed				|
    | B05			| password field empty| valid		| valid 		| empty				|	failed				|	
  