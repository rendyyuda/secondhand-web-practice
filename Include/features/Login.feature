@Login
Feature: Login

  Scenario Outline: User want to login with <condition>
  Given  User go to login page
  When User input email field with <email> email
  And User input password field with <password> password
  And User click Masuk button
  Then User <result> login
  
  Examples:
  | case_id | condition            | email   | password | result     		|
  | A01     | correct credential   | valid   | valid    | successfully	|
  | A02     | invalid password     | valid   | invalid  | failed      	|
  | A03     | invalid email        | invalid | invalid  | failed      	|
  | A04     | email field empty		 | empty   | valid    | failed      	|
  | A05     | password field empty | valid   | empty    | failed      	|