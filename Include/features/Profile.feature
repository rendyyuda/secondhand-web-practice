@Profile
Feature: Edit Profile

  Background: User already login
  	Given  User go to login page
    When User input email field with valid email
    And User input password field with valid password
    And User click Masuk button
    Then User successfully login

  @Positive_case
  Scenario: User want to update profile details with 
    When user click the icon in the very top right corner
    And user Choose Profile menu
    And user delete and enter new data in Name field with 'Leonia Bolloti'
    And user edit a City by clicking the City dropdown list '2'
    And user delete and enter new address in Address field 'Jln. Jalan aja kita kemana mau'
    And user delete and enter new phone number in Phone Number field '081389065789'
    And user click Submit button
    Then user can see successful pop up message
    
