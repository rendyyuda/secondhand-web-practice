Feature: edit product

  Background: user already login
    Given User go to login page
    When User input email field with valid email
    And User input password field with valid password
    And User click Masuk button
    And User successfully login

  Scenario: User want to edit product to their product list
    When User click daftar jual saya button
    And User click product card
    And User click edit button
    And User input product name
    And User input product price
    And User choose category
    And User input description
    And User click terbitkan button
    Then user can successfully edit product
