@positive_case
Feature: Bid-Product

  Background: user already login
    Given User go to login page
    When User input email field with "djakafarizki.fl@gmail.com"
    And User input password field with "berapahari05"
    And User click Masuk button
    Then User successfully login
     
   Scenario Outline: User can bid a product with <condition>
    When User click <product> product
    And User click button saya tertarik dan ingin nego
    And User input <bid_price> bid price
    And User click button kirim
    Then User <result> bid product
    
    Examples:
    | case_id  | condition             | product   									| bid_price  | result     	 |
    | A01     | lower price   			   | product_motor 							| '5000'       | successfully  |
    | A02     | higher price		       | product_patung   					| '12000000'   | successfully  |
    | A03      | 0 price        			 | product_patung_motor_orang | '0'			     | failed      	 |
    | A04     | alphabet price 			 | product_patung_motor_orang | 'asdasdsda'  | failed      	 |
    | A05     | -100 price   				 | product_patung_motor_orang | '-100'       | failed      	 |