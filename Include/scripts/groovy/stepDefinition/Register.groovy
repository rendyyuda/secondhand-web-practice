package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import org.apache.commons.lang3.RandomStringUtils


public class Register {
	@Given("User go to register page")
	public void user_go_to_register_page() {
		WebUI.openBrowser('')
		WebUI.maximizeWindow()
		WebUI.navigateToUrl('https://secondhand-store.herokuapp.com/')
		WebUI.click(findTestObject('Home_page_not_login/button_masuk'))
		WebUI.verifyElementVisible(findTestObject('login_page/banner_SecondhandLogin'))
		WebUI.verifyElementVisible(findTestObject('login_page/title_Masuk'))
		WebUI.click(findTestObject('login_page/button_Daftar'))
		WebUI.verifyElementVisible(findTestObject('Register_page/banner_Secondhand'))
		WebUI.verifyElementVisible(findTestObject('Register_page/title_Daftar'))
	}

	@Then("User successfully regist")
	public void user_succesfully_regist() {
		WebUI.verifyElementVisible(findTestObject('Register_page/message_VerifikasiEmail'))
	}

	@Then("User failed regist")
	public void user_failed_regist() {
		WebUI.verifyElementVisible(findTestObject('Register_page/banner_Secondhand'))
		WebUI.verifyElementVisible(findTestObject('Register_page/title_Daftar'))
	}

	@When("User input name field with valid name in Register Page")
	public void user_input_name_field_with_valid_name_in_Register_Page(){
		WebUI.setText(findTestObject('Register_page/input_Nama'), RandomStringUtils.randomAlphabetic(5))
	}

	@When("User input email field with valid email in Register Page")
	public void user_input_email_field_with_valid_email_in_Register_Page(){
		WebUI.setText(findTestObject('Register_page/input_Email'), RandomStringUtils.randomAlphanumeric(9) + "@gmail.com")
	}


	@When("User input email field with invalid email in Register Page")
	public void user_input_email_field_with_invalid_email_in_Register_Page() {
		WebUI.setText(findTestObject('Register_page/input_Email'), 'farisanurdiani@gmail.com')
	}

	@When("User input name field with empty name in Register Page")
	public void user_input_name_field_with_empty_name_in_Register_Page() {
		WebUI.setText(findTestObject('Register_page/input_Nama'), '')
	}

	@When("User input email field with empty email in Register Page")
	public void user_input_email_field_with_empty_email_in_Register_Page() {
		WebUI.setText(findTestObject('Register_page/input_Email'), '')
	}

	@When("User input password field with empty password in Register Page")
	public void user_input_password_field_with_empty_password_in_Register_Page() {
		WebUI.setText(findTestObject('Register_page/input_Password'), '')
	}

	@When("User input password field with valid password in Register Page")
	public void user_input_password_field_with_valid_password_in_Register_Page() {
		WebUI.setText(findTestObject('Register_page/input_Password'), 'hohoho12345')
	}

	@When("User click Daftar button")
	public void user_click_Daftar_button() {
		WebUI.click(findTestObject('Register_page/button_Daftar'))
	}
}