package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import internal.GlobalVariable

public class Add_Product {

	@Given("User click jual button")
	public void user_click_jual_button() {
		WebUI.click(findTestObject('Home_page_login/button_Jual'))
		WebUI.verifyElementVisible(findTestObject('Productinfo_page/button_Terbitkan'))
	}


	@When("User click terbitkan button")
	public void User_click_terbitkan_button() {
		WebUI.click(findTestObject('Productinfo_page/button_Terbitkan'))
	}


	@When("User input product name with {string}")
	public void user_input_product_name_with(String name) {
		WebUI.setText(findTestObject('Productinfo_page/input_NamaProduk'), name)
	}

	@When("User input product price with {string}")
	public void user_input_product_price_with(String price) {
		WebUI.setText(findTestObject('Productinfo_Page/input_HargaProduk'), price)
	}

	@When("User choose Kendaraan on product category")
	public void user_choose_Kendaraan_on_product_category() {
		WebUI.selectOptionByValue(findTestObject('Productinfo_page/select_Kategori'), '2', false)
	}

	@When("User input product description with description")
	public void user_input_product_description_with_description() {
		WebUI.setText(findTestObject('Productinfo_page/input_Deskripsi'), 'Tahan banting dan tahan api')
	}

	@Then("User successfully add product")
	public void user_successfully_add_product() {
		WebUI.verifyElementVisible(findTestObject('Home_page_login/banner_DiskonRamadhan'))
	}


	@Then("User failed add product")
	public void user_failed_add_product() {
		WebUI.verifyElementVisible(findTestObject('Productinfo_page/button_Terbitkan'))
	}

	@When("User input product price with empty")
	public void user_input_product_price_with_empty() {
		WebUI.setText(findTestObject('Productinfo_page/input_HargaProduk'), '')
	}

	@When("User input product description with empty")
	public void user_input_product_description_with_empty() {
		WebUI.setText(findTestObject('Productinfo_page/input_Deskripsi'), '')
	}

	@When("User choose empty on product category")
	public void user_choose_empty_on_product_category() {
		WebUI.verifyOptionNotSelectedByValue(findTestObject('Productinfo_page/select_Kategori'), '2', false, 0)
	}
}