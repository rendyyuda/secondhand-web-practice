package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import internal.GlobalVariable

public class Bid_Product {
	//	@When("User click product that their want")
	//	public void user_click_product_that_their_want() {
	//		WebUI.click(findTestObject('Home_page_login/button_pc_gaming'))
	//	}
	//
	//	@When("User click button saya tertarik dan ingin nego")
	//	public void user_click_button_saya_tertarik_dan_ingin_nego() {
	//		WebUI.click(findTestObject('Product_Page/button_Saya tertarik dan ingin nego'))
	//	}
	//
	//	@When("User input bid price {string}")
	//	public void user_input_bid_price(String string) {
	//		WebUI.setText(findTestObject('Product_Page/input_Harga Tawar_form-control'), string)
	//	}
	//
	//	@When("User click button kirim")
	//	public void user_click_button_kirim() {
	//		WebUI.click(findTestObject('Product_Page/button_Kirim'))
	//	}
	//
	//	@Then("User can see success pop up message")
	//	public void user_can_see_success_pop_up_message() {
	//		WebUI.verifyElementVisible(findTestObject('Product_Page/message_Harga tawarmu berhasil dikirim ke penjual'))
	//	}
	//
	//	@Then("User cannot bid product")
	//	public void user_cannot_bid_product() {
	//		WebUI.verifyElementVisible(findTestObject('Product_Page/message_Masukkan Harga Tawarmu'))
	//	}
	@When("User click product_motor product")
	public void user_click_product_motor_product() {
		WebUI.click(findTestObject('Home_page_login/product_MotorAsli'))

	}

	@When("User click button saya tertarik dan ingin nego")
	public void user_click_button_saya_tertarik_dan_ingin_nego() {
		WebUI.click(findTestObject('Product_Page/button_Saya tertarik dan ingin nego'))

	}

	@When("User input {string} bid price")
	public void user_input_bid_price(String bid_price) {
		WebUI.setText(findTestObject('Product_Page/input_Harga Tawar_form-control'), bid_price)

	}

	@When("User click button kirim")
	public void user_click_button_kirim() {
		WebUI.click(findTestObject('Product_Page/button_Kirim'))

	}

	@Then("User successfully bid product")
	public void user_successfully_bid_product() {
		WebUI.verifyElementVisible(findTestObject('Product_Page/message_Harga tawarmu berhasil dikirim ke penjual'))
	}

	@When("User click product_patung product")
	public void user_click_product_patung_product() {
		WebUI.click(findTestObject('Home_page_login/product_patung_motor'))

	}

	@When("User click product_patung_motor_orang product")
	public void user_click_product_patung_motor_orang_product() {
		WebUI.click(findTestObject('Home_page_login/product_patung_motor_orang'))

	}

	@Then("User failed bid product")
	public void user_failed_bid_product() {
		WebUI.verifyElementVisible(findTestObject('Product_Page/message_Masukkan Harga Tawarmu'))

	}

	//	@When("User input asdasdsda bid price")
	//	public void user_input_asdasdsda_bid_price() {
	//		WebUI.setText(findTestObject('Product_Page/input_Harga Tawar_form-control'), "asdasdsda")
	//
	//	}
}


