<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>message_EmailSudahDigunakan</name>
   <tag></tag>
   <elementGuidId>9be85956-6d47-4e8d-b28c-d662b69df73d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/div/div[2]/form/div/strong</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.alert.alert-danger.alert-dismissible.fade.show > strong</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>strong</value>
      <webElementGuid>cb67cd3c-0243-483e-9827-a375cfe3ad3d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Email sudah digunakan</value>
      <webElementGuid>9423fc88-cc67-4984-8c3b-4a55c6372677</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;App&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-sm-6 form-register&quot;]/form[1]/div[@class=&quot;alert alert-danger alert-dismissible fade show&quot;]/strong[1]</value>
      <webElementGuid>ea27568b-90e7-4654-b4fa-a25348cf8a22</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div/div[2]/form/div/strong</value>
      <webElementGuid>4993a9ac-d700-4137-8ada-ed75b3bf5fb4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Daftar'])[1]/following::strong[1]</value>
      <webElementGuid>8fc8c7cf-f793-46b6-8bc0-0918092dc343</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SecondHand.'])[1]/following::strong[2]</value>
      <webElementGuid>4a36fc8e-8657-45c0-b629-1c9459f09fba</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Nama'])[1]/preceding::strong[1]</value>
      <webElementGuid>1ad85217-fde7-4144-86f6-a152abe0ac1f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Email address'])[1]/preceding::strong[1]</value>
      <webElementGuid>5a5df329-a403-43d8-837d-53e25c7cf9b8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Email sudah digunakan']/parent::*</value>
      <webElementGuid>6d4615ef-dd48-446d-9db5-0c5219138e59</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/strong</value>
      <webElementGuid>b2dcbf4d-1fb4-453c-87e4-172a88345bf2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//strong[(text() = 'Email sudah digunakan' or . = 'Email sudah digunakan')]</value>
      <webElementGuid>c3ab97dd-2cb0-4baf-b55e-9c19eaa4c0de</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
