<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>title_Daftar</name>
   <tag></tag>
   <elementGuidId>f5b9a2cf-37ea-476c-b4ae-7c620be92bf3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/div/div[2]/form/h2/strong</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>strong</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>strong</value>
      <webElementGuid>771eb2ae-474f-453f-a3fc-bd91ac4a8c34</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Daftar</value>
      <webElementGuid>b382fdac-4936-4fcd-b066-d4c588ea9339</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;App&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-sm-6 form-register&quot;]/form[1]/h2[1]/strong[1]</value>
      <webElementGuid>3a7da795-00ad-4d5f-bd69-ce0403e7deba</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div/div[2]/form/h2/strong</value>
      <webElementGuid>49024e2f-4393-44c6-8149-cd11303cd505</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SecondHand.'])[1]/following::strong[1]</value>
      <webElementGuid>f416919a-339b-4022-a0c9-3f5b7fa92dc7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Silahkan verifikasi email agar dapat menggunakan layanan kami'])[1]/preceding::strong[1]</value>
      <webElementGuid>ddb066c6-0d39-4d23-8220-99ebbc830fd9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Nama'])[1]/preceding::strong[2]</value>
      <webElementGuid>916014cb-548f-43e8-9513-17f85b70f65e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Daftar']/parent::*</value>
      <webElementGuid>a5130ec3-f37b-4fc7-96bd-1b84433cf54d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//strong</value>
      <webElementGuid>a69e21d1-a49e-4f6b-871e-a2603f728bfd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//strong[(text() = 'Daftar' or . = 'Daftar')]</value>
      <webElementGuid>c8ae4601-dd55-43d6-b44b-a4a0ed625c43</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
