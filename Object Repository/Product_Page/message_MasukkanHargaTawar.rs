<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>message_Masukkan Harga Tawarmu</name>
   <tag></tag>
   <elementGuidId>7fcd0a8a-fd25-429e-bb31-29162e82ffa9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.modal-title.h4</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='`'])[2]/following::div[7]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>2a7a92d1-8e11-4442-ac7e-ae4a7598c904</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>modal-title h4</value>
      <webElementGuid>fc7dc170-6aeb-4a6c-9c6c-08213fe03410</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Masukkan Harga Tawarmu</value>
      <webElementGuid>16d76ae1-a2b4-4054-99fb-8df5208cb2ad</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;modal-open&quot;]/div[@class=&quot;fade modal show&quot;]/div[@class=&quot;modal-dialog modal-sm modal-dialog-centered&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-header&quot;]/div[@class=&quot;modal-title h4&quot;]</value>
      <webElementGuid>56a51577-51ce-42a3-ade3-016c81d25d90</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='`'])[2]/following::div[7]</value>
      <webElementGuid>723ca0d7-45e2-4aba-a525-347fc0548c3c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Alt'])[2]/following::div[7]</value>
      <webElementGuid>d444c381-986d-4741-b1f2-828037b4ecda</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Action Figures Anime'])[2]/preceding::div[1]</value>
      <webElementGuid>218ea05c-a3ed-4c17-aa64-3653b295645e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Harga Tawar'])[1]/preceding::div[4]</value>
      <webElementGuid>f7e65f01-f80a-4f2b-945a-ff6b86179f15</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Masukkan Harga Tawarmu']/parent::*</value>
      <webElementGuid>da54b49b-f802-4b7a-85a9-67cbae6bb74e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div/div/div</value>
      <webElementGuid>79fb2af0-c62f-409a-ad8d-26cd7f147947</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Masukkan Harga Tawarmu' or . = 'Masukkan Harga Tawarmu')]</value>
      <webElementGuid>4f2f25e1-873e-4d1e-b6c7-40fd3d46954d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
