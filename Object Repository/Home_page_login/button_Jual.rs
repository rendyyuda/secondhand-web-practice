<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Jual</name>
   <tag></tag>
   <elementGuidId>5183938e-2ddd-4af5-9344-ae85056562db</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@type='button']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a.btn.btn-color-theme.pl-3.pr-3.button-jual</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>5b3426e1-6014-4280-b255-09c56d7f385c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>6a1d3b0a-689a-4aad-88ee-36dc164736d7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-color-theme pl-3 pr-3 button-jual</value>
      <webElementGuid>3b143c73-764f-4c44-83ed-c577043d74bf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/productinfo</value>
      <webElementGuid>029556cb-be59-418d-85f3-ebe478a02521</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Jual</value>
      <webElementGuid>e5e4c5fe-473d-4c66-af5f-730a90b613a4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[1]/a[@class=&quot;btn btn-color-theme pl-3 pr-3 button-jual&quot;]</value>
      <webElementGuid>f9d4f814-3e4a-4c58-96c1-51ee51bf5556</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@type='button']</value>
      <webElementGuid>36813966-13d6-43ff-8553-c678f47dfe09</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/a</value>
      <webElementGuid>b95dc806-0ff7-4c69-bee7-61a30b4364ec</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Jual')]</value>
      <webElementGuid>a4d4097b-ffe9-4a6c-9228-d27b65855f1f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Capture object:'])[1]/preceding::a[1]</value>
      <webElementGuid>8e376e51-41db-4c23-bcef-6e82f451189d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Alt'])[1]/preceding::a[1]</value>
      <webElementGuid>83c152ac-1ca4-4252-bb24-f6c5e82f4669</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Jual']/parent::*</value>
      <webElementGuid>87f8d404-fc93-4d58-8a00-effee0e00df7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/productinfo')]</value>
      <webElementGuid>cad8984b-7828-4d25-b8b9-7f9202f232ce</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/a</value>
      <webElementGuid>742aab68-b598-459a-8a66-c1a0476412b1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@type = 'button' and @href = '/productinfo' and (text() = ' Jual' or . = ' Jual')]</value>
      <webElementGuid>c5ada057-e76d-49ee-9be8-b6174224fcfd</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
